# markdownlint-cli2

A fast, flexible, configuration-based command-line interface for linting
Markdown/CommonMark files with the markdownlint library
[markdownlint-cli2](https://github.com/DavidAnson/markdownlint-cli2).

## Using in the Pipeline

To enable markdownlint-cli2 in your project's pipeline add
`markdownlint-cli2` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "markdownlint-cli2"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install the [markdownlint extension](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
for Visual Studio Code.

## Using Locally in Your Development Environment

markdownlint-cli2 can be run locally in a linux-based bash shell or in a
linting shell script with this Docker command:

```bash
docker run --rm -v "${PWD}:/app/project"  -w /app/project 
    registry.gitlab.com/pipeline-components/markdownlint-cli2:latest 
    markdownlint-cli2
```

## Configuration

Each project should have a `.markdownlint-cli2.yaml` file which is used for
[markdownlint-cli2 configuration](https://github.com/DavidAnson/markdownlint-cli2#markdownlint-cli2yaml).

This file is used by the pipeline tool, the Visual Studio Code extension,
and the locally run Docker image.

This project has a `.markdownlint-cli2.yaml` file that can be used as an example.

## Licensing

- Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
- Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
- Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
